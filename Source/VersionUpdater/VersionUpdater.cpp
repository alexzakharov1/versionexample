#include <stdio.h>

void main( int argc , char** argv )
{
	if ( argc < 2 )
		return;
	const char* path = argv[ 1 ];
	int major , minor , branch , build;

	FILE* ptr_file = fopen( path , "r" );
	fscanf( ptr_file , "%d.%d.%d.%d" , &major , &minor , &branch , &build );
	fclose( ptr_file );

	build++;

	ptr_file = fopen( path , "w" );
	fprintf( ptr_file , "%d.%d.%d.%d" , major , minor , branch , build );
	fclose( ptr_file );
}